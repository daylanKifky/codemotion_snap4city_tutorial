# SNAP4CITY Codemotion IOT tutorials

[Snap4city](https://www.snap4city.org) is the new platform for the sentient city. By using it you can get direct access to data from thousands of real sensors deployed on several cities, together with other types of resources such as static databases containing bus stops, wifi hotspots and many others.

Adding a custom sensor to the network for private or public use is a straightforward process. In these tutorials we will go through the steps required to connect a raspberry Pi and an ESP12 to the snap4city network and send the readings of a temperature and humidity sensor.

An IOT application on the snap4city platform will be used to output the real time sensor values to a public dashboard.

![snap4city_scheme](snap4city_world_dashboard.png)

## Tutorial 01: Raspberry Pi as a temperature and humidity sensor

Text version [here](#).

![wiring raspberry and dht11](raspberry_DHT11.png)

files:
- **install_s4c_deps.sh**: install dependencies and configure your raspberry to behave as an IOT device on the snap4city network
- **rasspberry_sensor_iot.json**: nodeRed flow running on the raspberry. It reads the values from a DHT11 sensor and uploads them to a remote IOT broker
- **dashboard_iot_app.json**: nodeRed flow for the IOT application running on the snap4city platform. It monitors the values from the iot sensor and displays them on a dashboard.

## Tutorial 02: ESP-12 as a temperature and humidity sensor

Text version [here](#).

![wiring raspberry and dht11](ESP12_DHT11.png)

- **snap4city_device folder**: Arduino sketch to program the ESP12.
- **dashboard_iot_app.json**: nodeRed flow for the IOT application running on the snap4city platform. It monitors the values from the iot sensor and displays them on a dashboard.