#!/bin/bash
#
# Install dependencies and configure your raspberry to behave 
# as an IOT device on the snap4city platform.
#	
# This file is part of the codemotion IOT snap4city tutorials
# Copyright (c) 2019 Bruno Laurencich
# 
# Distributed under the MIT License
#

sudo apt update

# -----------  install nodeRed  -----------
bash <(curl -sL https://raw.githubusercontent.com/node-red/raspbian-deb-package/master/resources/update-nodejs-and-nodered)

# -----------  install the BCM2835 c library  -----------
#Needed to access the low level peripherals on the raspberry pi
#from the DHT sensor library

BCMLIB=bcm2835-1.58
BCMTAR=.tar.gz

cd /tmp
wget http://www.airspayce.com/mikem/bcm2835/$BCMLIB$BCMTAR 
tar zxvf $BCMLIB$BCMTAR
cd $BCMLIB
./configure
make
sudo make check
sudo make install

# -----------  Install the nodeRed node packages needed  -----------
cd ~/.node-red

#DHT sensor library
sudo npm install --unsafe-perm -g node-dht-sensor
sudo npm install --unsafe-perm -g node-red-contrib-dht-sensor

#SNAP4CITY nodes
npm install node-red-contrib-snap4city-developer
npm install node-red-contrib-snap4city-user


# -----------  Change the hostname to sc4device  -----------
# Allows to access the device from the network by using "s4cdevice.local"
# Only works if the client PC has a zeroconfig client (most linux distros and mac)

sudo bash -c 'echo s4cdevice > /etc/hostname' 

# -----------  Enable and run the nodeRed service  -----------

cd
sudo systemctl enable nodered.service 
node-red-start 