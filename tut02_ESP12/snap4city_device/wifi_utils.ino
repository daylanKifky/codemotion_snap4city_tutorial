#define TEST_HOST "google.com"
#define TEST_HOST_PORT 80



void connectWifi() {
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

String getTestRequest() {
  return String("GET /") + " HTTP/1.1\r\n" +
         "Host: " + TEST_HOST + "\r\n" +
         "Connection: close\r\n" +
         "\r\n";
}

bool testRemoteConnection(bool print_response) {
  Serial.print("Testing connection to ");
  Serial.print(TEST_HOST);
  Serial.print(':');
  Serial.println(TEST_HOST_PORT);

  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  if (!client.connect(TEST_HOST, TEST_HOST_PORT)) {
    Serial.println("Test connection failed.\n Does the wifi network has internet access?");
    Serial.println("Retrying in 5 seconds.");
    client.stop();
    delay(5000);
    return false;
  }

  client.print(getTestRequest());

  // wait for data to be available
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Test connection Timeout!");
      Serial.println("Retrying in 10 seconds.");

      client.stop();
      delay(5000);
      return false ;
    }
  }

  if (print_response) {
    // Read all the lines of the reply from server and print them to Serial
    Serial.println("===== Receiving from remote server ====");
    // not testing 'client.connected()' since we do not need to send data here
    while (client.available()) {
      char ch = static_cast<char>(client.read());
      Serial.print(ch);
    }
    Serial.println("===== End response from remote server ====");
  }

  Serial.println("Test connection successful.");
  client.stop();
  return true;


  // // Close the connection
  // Serial.println();
  // Serial.println("closing connection");
  // client.stop();
}
