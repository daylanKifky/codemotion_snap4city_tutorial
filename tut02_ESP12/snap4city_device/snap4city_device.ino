/**
Arduino code for an ESP12 device. 
It reads the values from a DHT11 sensor and uploads them to a remote IOT broker.

# This file is part of the codemotion IOT snap4city tutorials
# Copyright (c) 2019 Bruno Laurencich
# 
# Distributed under the MIT License
#
*/


#include <ESP8266WiFi.h>
#include <WiFiClientSecureBearSSL.h>
#include <ESP8266HTTPClient.h>
#include "DHTesp.h"

#ifndef STASSID
#define STASSID "<< YOUR WIFI SSID >>" // <--- Set your WIFI's name  
#define STAPSK  "<< YOUR WIFI PASSWORD >>" // <--- Set your WIFI's password
#endif


#define S4C_BROKER_URI    "broker1.snap4city.org"
#define S4C_BROKER_PORT   8080
#define S4C_SHA1          "C61E32DBFAE7F14C0B10177F2D2300843C41C550"

// !!! --------- Set your devices attributes ------- !!!
// They were set during registration on the snap4city platform  
#define S4C_DEVICE_ID     "<< YOUR DEVICE NAME ID >>"  
#define S4C_DEVICE_TYPE   "Ambiental"
#define S4C_K1            "<< YOUR DEVICE KEY 1 >>"
#define S4C_K2            "<< YOUR DEVICE KEY 2 >>"

//Should the ESP12 print the response from the server to the serial console? 
#define PRINT_BROKER_RESPONSE false

const char* ssid     = STASSID;
const char* password = STAPSK;

DHTesp dht;

// digital pin the DHT11 is conected to
#define DHTPIN 4     
// there are multiple kinds of DHT sensors, uncomment the one that you are using.
#define DHTTYPE DHTesp::DHT11 
// #define DHTTYPE DHTesp::DHT22 


void setup() {
  Serial.begin(115200);

  connectWifi();

  bool internet_working = false;
  while (!internet_working) {
    internet_working = testRemoteConnection(false);
  }

  dht.setup(DHTPIN, DHTTYPE);
  return;
}

void loop() {
  TempAndHumidity lastValues = dht.getTempAndHumidity();
  Serial.println("Temperature: " + String(lastValues.temperature,0));
  Serial.println("Humidity: " + String(lastValues.humidity,0));

  BearSSL::WiFiClientSecure client;

  String url = buildBrokerURL();
  Serial.printf("Attempting to connect to %s\n", url.c_str());
  Serial.printf("Expected SHA1 fingerprint from server certificate '%s'\n", S4C_SHA1);
  client.setFingerprint(S4C_SHA1);

  HTTPClient https;
  if (https.begin(client, url)) {

    https.addHeader("User-Agent", "Snap4city ESP-12 Codemotion tutorial");
    https.addHeader("Content-Type", "application/json");
    https.addHeader("Connection", "close");

    String data = buildMessage( lastValues.temperature , lastValues.humidity);

    Serial.print("[HTTPS] POST request sent\n");
    int httpCode = https.POST(data);

    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTPS] POST... code: %d\n", httpCode);

      // file found at server
      if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
        String payload = https.getString();
        if (PRINT_BROKER_RESPONSE){
          Serial.println("===== Answer from broker ====");
          Serial.println(payload);
          Serial.println("===== End answer from broker ====");
        }

      } else {
        Serial.printf("##### Error from broker: %s ####", httpCode);
      }

    } else { 
      //https.POST() failed
      Serial.printf("[HTTPS] POST... failed, error: %s\n", https.errorToString(httpCode).c_str());
    }

    https.end();
  } else {
    Serial.printf("[HTTPS] Unable to connect\n");
  }

  Serial.println("Retrying in 5 seconds.");
  delay(5000);
  return;

}
