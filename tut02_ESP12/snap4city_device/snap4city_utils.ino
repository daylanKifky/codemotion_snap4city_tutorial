String buildBrokerURL(){
  String req = "https://";
  req += S4C_BROKER_URI;
  req += ":";
  req += S4C_BROKER_PORT;
  req += "/v1/updateContext?elementid=";
  req += S4C_DEVICE_ID;
  req += "&k1=";
  req += S4C_K1;
  req += "&k2=";
  req += S4C_K2;
  return req;
}


String buildMessage(float temperature, float humidity){
  String msg = "{\"contextElements\": [{\"type\": \"";
  msg += S4C_DEVICE_TYPE;
  msg += "\",\"isPattern\": \"false\",\"id\": \"";
  msg += S4C_DEVICE_ID;
  msg += "\",\"attributes\": [{\"name\": \"temperature\",\"type\": \"float\",\"value\": \"";
  msg += temperature;
  msg += "\"},{\"name\": \"humidity\",\"type\": \"float\",\"value\": \"";
  msg += humidity;
  msg += "\"}]}],\"updateAction\": \"APPEND\"}";
  return msg;
}

